## Lösungsansatz für die Herausforderung 01_025_1861_#Stubenzocker des #WirVsVirus Hackathons.
---
### 2 Minuten Pitch Video

<a href="http://www.youtube.com/watch?feature=player_embedded&v=K3gM_nwvvD0
" target="_blank"><img src="https://img.youtube.com/vi/K3gM_nwvvD0/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

[https://www.youtube.com/watch?v=K3gM_nwvvD0](https://www.youtube.com/watch?v=K3gM_nwvvD0 "2 Minuten Pitch-Video")


### Inspiration
Geschlossene Geschäfte, leere Straßen und nur das eigene Spiegelbild das einen ständig ansieht. Leben hauptsächlich innerhalb der eigenen vier Wände ist eine große Herausforderungen für viele Menschen. Wir wollen helfen dieses mittels spielerischer Aufgaben zu erleichtern.

### What it does
Murmeltier stellt jeden täglich vor Aufgaben aus verschiedenen wählbaren Kategorien, von Fitness über Rätseln zu einfachen zwischenmenschlichen Aufgaben wie: Ruf deine Oma an. Jede erfolgreich gelöste bringt Punkte, um die schwierigeren Aufgaben freizuschalten und natürlich für den ersten Platz im Familienmodus. Eigene Herausforderungen können erstellt werden, um sich selbst, seine Familie oder die Freunde auf Trap zu halten. 
Für die Hardcore Gamer gibt es den Murmeltiermodus, hier werden keine Erfolge gespeichert und man beginnt jeden Tag von **vorn**. 

### How we built it
Mit viel Liebe und Leidenschaft aber vor allem auf Unity mit C#.

### Challenges we ran into

### Accomplishments that we're proud of
Es läuft schonmal.

### What we learned
Viel über Slack

### What's next for Murmeltier - Ein (Im-)Mobile Game
Die erste Erweiterung des Prototypen wäre zunächst eine größere Menge an Herausforderungen. Die vorhandenen Schiewrigkeitsgrade sind simpel auf einen größeren Maßstab skalierbar. Eine Vernetzung mit Freunden und Bekannten mittels sozialen Netzwerken würde es ermöglichen sich gegenseitig Herausforderungen zu stellen. Eine Zusammenarbeit mit Lernplattformen ist denkbar, um die Zahl der Lernspiele für Groß und Klein quasi ins Unendliche zu steigern.  