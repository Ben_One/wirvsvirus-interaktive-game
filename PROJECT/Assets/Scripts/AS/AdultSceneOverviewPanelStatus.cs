﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AdultSceneOverviewPanelStatus : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI progressText;
    [SerializeField] private GameTaskCategory panelCategory;
    [SerializeField] private GameTaskCategoryManager gtcm;

    private void LateUpdate()
    {
        UpdateStatusBar();
    }

    void UpdateStatusBar()
    {
        int progressPercentage = gtcm.GetCompletionState();
        progressText.text = "Aktuell: " + progressPercentage + "%";
    }
}
