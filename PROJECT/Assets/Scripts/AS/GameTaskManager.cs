﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTaskManager : MonoBehaviour
{

    GameTaskCategoryManager[] gameTaskCategoryManagers;

    private void Start()
    {
        gameTaskCategoryManagers = GetComponentsInChildren<GameTaskCategoryManager>();
    }

    public int GetCompletionStatus(GameTaskCategory category)
    {
        foreach(GameTaskCategoryManager gtcm in gameTaskCategoryManagers)
        {
            if(gtcm.Category.Equals(category)) {
                return gtcm.GetCompletionState();
            }
            
        }
        return 0;
    }
}
