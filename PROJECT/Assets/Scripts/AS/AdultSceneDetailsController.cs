﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AdultSceneDetailsController : MonoBehaviour
{
    [SerializeField] private Image progress;
    [SerializeField] public TextMeshProUGUI taskText;
    [SerializeField] public TextMeshProUGUI stepText;
    private GameTask activeGameTask;

    private GameTaskCategoryManager gameTaskCategoryManager;

    public GameTask ActiveGameTask { get => activeGameTask; set => activeGameTask = value; }

    public void SetGameTaskCategoryManager(GameTaskCategoryManager gtcn) {
        gameTaskCategoryManager = gtcn;
        ActiveGameTask = gtcn.GetActiveTask();
        RefreshAll();
    }

    private void RefreshAll()
    {
        CheckTaskProgress();
        UpdateText();
        UpdateProgressBar();

    }

    private void UpdateProgressBar()
    {
        int progressPercentage = Mathf.RoundToInt(((float)(ActiveGameTask.ActiveStep + 1) / ActiveGameTask.TotalSteps) * 100);
        if (this.progress != null)
        {
            progress.fillAmount = progressPercentage / 100.0f;
        }
        else
        {
            Debug.LogError("Progressbar not found");
        }
    }

    private void CheckTaskProgress()
    {
        if (ActiveGameTask.IsCompleted)
        {
            ActiveGameTask = gameTaskCategoryManager.GetNextTask();
        }
    }

    private void UpdateText()
    {
        if (taskText != null)
        {
            taskText.text = ActiveGameTask.TaskText + " (Teilschritt " + (ActiveGameTask.ActiveStep + 1) + " von " + ActiveGameTask.TotalSteps + ")";
        }
        else
        {
            Debug.LogError("Tasktext not found");
        }
        if (stepText != null)
        {
            stepText.text = ActiveGameTask.StepText;
        }
        else
        {
            Debug.LogError("Steptext not found");
        }
    }

    public void ProgressActiveTask()
    {
        ActiveGameTask.ActiveStep++;
        Debug.Log("Step:" + ActiveGameTask.ActiveStep + " of " + ActiveGameTask.TotalSteps);
        RefreshAll();
    }
}
