﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameTasks/AdultGameTask")]
public class GameTask : ScriptableObject
{
    [SerializeField] private int activeStep = 0;
    [SerializeField] private bool isCompleted = false;
    [SerializeField] private List<GameTaskStep> steps;
    [SerializeField] private string taskText = "";

    public int TotalSteps { get => steps.Count;}
    public int ActiveStep
    {
        get => activeStep; set
        {
            activeStep = value;
            if(activeStep >= TotalSteps)
            {
                activeStep = 0;
                isCompleted = true;
                Debug.Log("Task completed");
            }
        }
    }
    public bool IsCompleted { get => isCompleted; set => isCompleted = value; }
    public string StepText {
        get => steps[activeStep].GameTaskText;
    }
    public string TaskText { get => taskText;}
}
