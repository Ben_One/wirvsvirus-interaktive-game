﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewSwitchController : MonoBehaviour
{
    [SerializeField] GameObject overview;
    [SerializeField] GameObject details;

    public void SwitchToDetailsView(GameTaskCategoryManager gtcm)
    {
        overview.SetActive(false);
        details.SetActive(true);
        details.GetComponent<AdultSceneDetailsController>().SetGameTaskCategoryManager(gtcm);
    }

    public void SwitchToOverviewView()
    {
        overview.SetActive(true);
        details.SetActive(false);
    }
}
