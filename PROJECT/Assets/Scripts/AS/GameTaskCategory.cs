﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameTaskCategory
{
   LEARN,
   SPORTS,
   OTHER
}
