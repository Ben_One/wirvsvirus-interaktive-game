﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameTasks/AdultGameTaskStep")]
public class GameTaskStep : ScriptableObject
{
    [SerializeField] private string gameTaskText = "";

    public string GameTaskText { get => gameTaskText; set => gameTaskText = value; }
}
