﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTaskCategoryManager : MonoBehaviour
{
    [SerializeField] GameTaskCategory category;

    [SerializeField] List<GameTask> tasks;
    private int activeTaskCounter;

    public GameTaskCategory Category { get => category; set => category = value; }

    public GameTask GetNextTask()
    {
        activeTaskCounter++;
        if (activeTaskCounter >= tasks.Count)
        {
            activeTaskCounter = 0;
        }
        return tasks[activeTaskCounter];
    }

    public GameTask GetActiveTask()
    {
        return tasks[activeTaskCounter];
    }

    public int GetCompletionState()
    {
        int completedCount = 0;
        foreach (GameTask t in tasks)
        {
            if(t.IsCompleted)
            {
                completedCount++;
            }
        }
        return Mathf.RoundToInt(((float)(completedCount) / tasks.Count) * 100);
    }

}
