﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace MobileUtilities
{
    public static class StorageUtility
    {
#if PLATFORM_ANDROID
        /// <summary>
        /// Loads a file from the streaming assets folder.
        /// </summary>
        /// <param name="_path">Path to a file inside the streaming assets folder.</param>
        /// <returns>The content of the file.</returns>
        public static string LoadFromStreamingAssets(string _path)
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(_path))
            {
                AsyncOperation request = webRequest.SendWebRequest();
                while (!request.isDone) { }

                return webRequest.downloadHandler.text;
            }
        }
#else
    /// <summary>
    /// Loads a file from the streaming assets folder.
    /// </summary>
    /// <param name="_path">Path to a file inside the streaming assets folder.</param>
    /// <returns>The content of the file.</returns>
    public static string LoadFromStreamingAssets(string _path)
    {
        if (!File.Exists(_path))
            return null;

        using (FileStream fs = File.OpenRead(_path))
        {
            using (StreamReader sr = new StreamReader(fs))
            {
                return sr.ReadToEnd();
            }
        }
    }
#endif
    }
}