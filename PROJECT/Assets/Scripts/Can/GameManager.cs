﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameManager : MonoBehaviour
{
    #region -variables and components-
        public static GameManager instance { get; private set; }
    [SerializeField]
    private TextMeshProUGUI timeGO;
    #endregion 

    /// <summary>
    /// In Awake, if the GameManager is instaciated it will be destroyed and then it instanciates one. 
    /// </summary>
    void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        instance = this;
    }

    void Start()
    {
        
    }

    void Update()
    {
        CurrentTime();
    }

    void CurrentTime()
    {
        timeGO.SetText(System.DateTime.Now.ToString("HH:mm"));
    }
}
