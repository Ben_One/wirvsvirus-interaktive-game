﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom_In_Repeat : MonoBehaviour
{
    #region -variables and components-
    Vector3 tmpScale;

    [SerializeField]
    private float duration = 1;
    [SerializeField]
    private float delay;
    [SerializeField]
    private bool setRandomDelay = false;
    [SerializeField]
    private LeanTweenType easeTpye;
    #endregion 

    void Start()
    {
        if (setRandomDelay) 
            delay = Random.value / 6;

        tmpScale = this.gameObject.transform.localScale;
        ZoomIn();
    }

    public void ZoomIn()
    {
        LeanTween.scale(gameObject, new Vector3(tmpScale.x / 2, tmpScale.y / 2, tmpScale.z / 2), duration).setDelay(delay).setEase(easeTpye).setOnComplete(ZoomOut);
    }

    public void ZoomOut()
    {
        LeanTween.scale(this.gameObject, new Vector3(tmpScale.x, tmpScale.y, tmpScale.z), duration).setDelay(delay).setEase(easeTpye).setOnComplete(ZoomIn);
    }

}
