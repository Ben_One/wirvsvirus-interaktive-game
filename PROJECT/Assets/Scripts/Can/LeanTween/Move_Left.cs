﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_Left : MonoBehaviour
{
    #region -variables and components-
    Vector3 tmpPos;

    [SerializeField]
    private float duration = 0.1f;
    [SerializeField]
    private float delay;
    [SerializeField]
    private bool setRandomDelay = false;
    [SerializeField]
    private LeanTweenType easeTpye;
    #endregion 

    void Start()
    {
        if (setRandomDelay)
            delay = Random.value / 6;

        tmpPos = this.gameObject.transform.localPosition;
        gameObject.transform.localPosition = new Vector3(300, tmpPos.y, tmpPos.z);
        FallingLeft();
    }

    public void FallingLeft()
    {
        LeanTween.moveLocalX(gameObject, 0, duration).setDelay(delay).setEase(easeTpye);
    }
}
