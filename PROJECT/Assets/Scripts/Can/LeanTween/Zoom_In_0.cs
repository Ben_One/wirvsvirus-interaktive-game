﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom_In_0 : MonoBehaviour
{
    #region -variables and components-
    Vector3 tmpScale;

    [SerializeField]
    private float duration = 0.15f;
    [SerializeField]
    private float delay;
    [SerializeField]
    private bool setRandomDelay = false;
    [SerializeField]
    private LeanTweenType easeTpye;
    #endregion 

    void Start()
    {
        if (setRandomDelay)
            delay = Random.value / 5;

        tmpScale = this.gameObject.transform.localScale;
        gameObject.transform.localScale = new Vector3(0, 0, 0);
        ZoomingIn();
    }

    public void ZoomingIn()
    {
        LeanTween.scale(gameObject, new Vector3(tmpScale.x, tmpScale.y, tmpScale.z), duration).setDelay(delay).setEase(easeTpye);
    }

}
