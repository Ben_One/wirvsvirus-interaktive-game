﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    #region -variables and components-
    #endregion

    /// <summary>
    /// Limimtes the FPS to 120, to prevent bugs in calculation by over 1000Fps.
    /// </summary>
    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    public void ChangeScene(int _sceneNumber)
    {
        SceneManager.LoadScene(_sceneNumber);

    }

    public void ChangeSceneWithName(string _sceneName)
    {
        SceneManager.LoadScene(_sceneName);
    }

    public void NextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }

    public void Quit()
    {
        Application.Quit();
    }
}
