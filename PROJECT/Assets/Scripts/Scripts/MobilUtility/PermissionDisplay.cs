﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class PermissionDisplay : MonoBehaviour
{
    public string Reason
    {
        set
        {
            m_reasonText.text = value;
        }
    }

    public System.Action ButtonMethod
    {
        set
        {
            m_button.onClick.AddListener(new UnityEngine.Events.UnityAction(value));
        }
    }

    private TextMeshProUGUI m_reasonText;
    private Button m_button;

    public static PermissionDisplay Create(string _permission, string _reason)
    {
        PermissionDisplay displayPrefab = Resources.Load<GameObject>("UI/PermissionDisplay").GetComponent<PermissionDisplay>();
        if (displayPrefab is object)
        {
            PermissionDisplay display = Instantiate(displayPrefab);
            display.Reason = _reason;
            display.ButtonMethod = () => RequestPermission(_permission, display.gameObject);
            return display;
        }
        return null;
    }

    private void Awake()
    {
        m_reasonText = GetComponentInChildren<TextMeshProUGUI>();
        m_button = GetComponentInChildren<Button>();
    }

    private static void RequestPermission(string _permission, GameObject _go)
    {
#if PLATFORM_ANDROID
        Permission.RequestUserPermission(_permission);
#endif
        Destroy(_go);
    }
}
