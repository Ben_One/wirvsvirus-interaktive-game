﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

namespace MobileUtilities
{
    public static class PermissionUtility
    {
        public static void RequestPermission(string _permission, string _reason)
        {
#if PLATFORM_ANDROID
            if (!Permission.HasUserAuthorizedPermission(_permission))
            {
                PermissionDisplay display = PermissionDisplay.Create(_permission, _reason);
            }
#endif
        }
    }
}
