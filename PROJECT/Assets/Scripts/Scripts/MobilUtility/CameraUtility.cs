﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

namespace MobileUtilities
{
    public static class CameraUtility
    {
#if PLATFORM_ANDROID
        public static bool HasPermission { get => Permission.HasUserAuthorizedPermission(Permission.Camera); }
#endif

        public static void RequestPermission(string _reason)
        {
#if PLATFORM_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
#if DEBUG
                Debug.Log("Permission not granted!");
#endif
                PermissionUtility.RequestPermission(Permission.Camera, _reason);
            }
            else
            {
#if DEBUG
                Debug.Log("Permission already granted!");
#endif
            }
#endif
        }


        public static WebCamTexture GetFrontFacingCamera()
        {
            string device = null;

            foreach (WebCamDevice webCam in WebCamTexture.devices)
            {
                if (webCam.isFrontFacing)
                {
                    device = webCam.name;
                    break;
                }
            }

            if (device is null)
            {
                return null;
            }

            return new WebCamTexture(device, Screen.currentResolution.width, Screen.currentResolution.height);
        }

        public static WebCamTexture GetBackFacingCamera()
        {
            string device = null;

            foreach (WebCamDevice webCam in WebCamTexture.devices)
            {
                if (!webCam.isFrontFacing)
                {
                    device = webCam.name;
                    break;
                }
            }

            if (device is null)
            {
                return null;
            }

            return new WebCamTexture(device, Screen.currentResolution.width, Screen.currentResolution.height);
        }
    }
}
