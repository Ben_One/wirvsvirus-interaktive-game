﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManagerSubtraktion : MonoBehaviour
{
    #region SerializeField
    [SerializeField]
    TextMeshProUGUI textOutput;

    [SerializeField]
    ButtonA buttonA;

    [SerializeField]
    TextMeshProUGUI buttonTextA;

    [SerializeField]
    ButtonB buttonB;

    [SerializeField]
    TextMeshProUGUI buttonTextB;

    [SerializeField]
    ButtonC buttonC;

    [SerializeField]
    TextMeshProUGUI buttonTextC;

    [SerializeField]
    ButtonD buttonD;

    [SerializeField]
    TextMeshProUGUI buttonTextD;
    #endregion

    //Select Level
    int levelSelect;
    void Start()
    {
        levelSelect = 1;
    }

    void Update()
    {
        #region Level Select
        if (levelSelect == 1)
        {
            level1();
        }

        if (levelSelect == 2)
        {
            level2();
        }

        if (levelSelect == 3)
        {
            level3();
        }

        if (levelSelect == 4)
        {
            level4();
        }

        if (levelSelect == 5)
        {
            level5();
        }

        if (levelSelect == 6)
        {
            level6();
        }

        if (levelSelect == 7)
        {
            level7();
        }

        if (levelSelect == 8)
        {
            level8();
        }

        if (levelSelect == 9)
        {
            level9();
        }

        if (levelSelect == 10)
        {
            level10();
        }
        #endregion
    }

    #region Level 1-10
    public void level1()
    {
        string result = "5";
        textOutput.text = "Bitte löse folgende Aufgabe:\n10 - 5 = ?";
        buttonTextA.text = "5";
        buttonTextB.text = "4";
        buttonTextC.text = "6";
        buttonTextD.text = "3";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 2;
        }

    }

    private void level2()
    {
        string result = "13";
        textOutput.text = "Bitte löse folgende Aufgabe:\n25 - 12 = ?";
        buttonTextA.text = "15";
        buttonTextB.text = "10";
        buttonTextC.text = "13";
        buttonTextD.text = "17";

        if (result == buttonC.buttoninputC)
        {
            levelSelect = 3;
        }
    }

    private void level3()
    {
        string result = "29";
        textOutput.text = "Bitte löse folgende Aufgabe:\n67 - 38 = ?";
        buttonTextA.text = "31";
        buttonTextB.text = "33";
        buttonTextC.text = "25";
        buttonTextD.text = "29";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 4;
        }
    }

    private void level4()
    {
        string result = "34";
        textOutput.text = "Bitte löse folgende Aufgabe:\n51 - 17 = ?";
        buttonTextA.text = "36";
        buttonTextB.text = "34";
        buttonTextC.text = "31";
        buttonTextD.text = "30";

        if (result == buttonB.buttoninputB)
        {
            levelSelect = 5;
        }
    }

    private void level5()
    {
        string result = "11";
        textOutput.text = "Bitte löse folgende Aufgabe:\n26 - 15 = ?";
        buttonTextA.text = "11";
        buttonTextB.text = "9";
        buttonTextC.text = "13";
        buttonTextD.text = "10";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 6;
        }
    }

    private void level6()
    {
        string result = "42";
        textOutput.text = "Bitte löse folgende Aufgabe:\n100 - 58 = ?";
        buttonTextA.text = "40";
        buttonTextB.text = "39";
        buttonTextC.text = "53";
        buttonTextD.text = "42";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 7;
        }
    }

    private void level7()
    {
        string result = "37";
        textOutput.text = "Bitte löse folgende Aufgabe:\n87 - 50 = ?";
        buttonTextA.text = "38";
        buttonTextB.text = "40";
        buttonTextC.text = "37";
        buttonTextD.text = "43";

        if (result == buttonC.buttoninputC)
        {
            levelSelect = 8;
        }
    }

    private void level8()
    {
        string result = "24";
        textOutput.text = "Bitte löse folgende Aufgabe:\n43 - 19 = ?";
        buttonTextA.text = "24";
        buttonTextB.text = "23";
        buttonTextC.text = "26";
        buttonTextD.text = "22";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 9;
        }
    }

    private void level9()
    {
        string result = "46";
        textOutput.text = "Bitte löse folgende Aufgabe:\n73 - 27 = ?";
        buttonTextA.text = "50";
        buttonTextB.text = "47";
        buttonTextC.text = "45";
        buttonTextD.text = "46";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 10;
        }
    }

    private void level10()
    {
        string result = "4";
        textOutput.text = "Bitte löse folgende Aufgabe:\n55 - 51 = ?";
        buttonTextA.text = "5";
        buttonTextB.text = "9";
        buttonTextC.text = "4";
        buttonTextD.text = "7";

        if (result == buttonC.buttoninputC)
        {
            textOutput.text = "Herzlichen Glückwunsch du hast alle 10 Aufgaben gemeistert :D";
            buttonTextA.text = " ";
            buttonA.enabled = false;
            buttonTextB.text = " ";
            buttonB.enabled = false;
            buttonTextC.text = " ";
            buttonC.enabled = false;
            buttonTextD.text = " ";
            buttonD.enabled = false;
        }
    }
    #endregion
}
