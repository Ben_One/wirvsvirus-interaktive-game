﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonA : MonoBehaviour
{
    public string buttoninputA;

    public void ButtonInput()
    {
        buttoninputA = GetComponentInChildren<TextMeshProUGUI>().text;
    }
}
