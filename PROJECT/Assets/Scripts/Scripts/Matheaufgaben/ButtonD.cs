﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonD : MonoBehaviour
{
    public string buttoninputD;

    public void ButtonInput()
    {
        buttoninputD = GetComponentInChildren<TextMeshProUGUI>().text;
    }
}
