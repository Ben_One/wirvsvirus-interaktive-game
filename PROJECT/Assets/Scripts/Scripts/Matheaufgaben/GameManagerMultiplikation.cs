﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManagerMultiplikation : MonoBehaviour
{
    #region SerializeField
    [SerializeField]
    TextMeshProUGUI textOutput;

    [SerializeField]
    ButtonA buttonA;

    [SerializeField]
    TextMeshProUGUI buttonTextA;

    [SerializeField]
    ButtonB buttonB;

    [SerializeField]
    TextMeshProUGUI buttonTextB;

    [SerializeField]
    ButtonC buttonC;

    [SerializeField]
    TextMeshProUGUI buttonTextC;

    [SerializeField]
    ButtonD buttonD;

    [SerializeField]
    TextMeshProUGUI buttonTextD;
    #endregion

    //Select Level
    int levelSelect;
    void Start()
    {
        levelSelect = 1;
    }

    void Update()
    {
        #region Level Select
        if (levelSelect == 1)
        {
            level1();
        }

        if (levelSelect == 2)
        {
            level2();
        }

        if (levelSelect == 3)
        {
            level3();
        }

        if (levelSelect == 4)
        {
            level4();
        }

        if (levelSelect == 5)
        {
            level5();
        }

        if (levelSelect == 6)
        {
            level6();
        }

        if (levelSelect == 7)
        {
            level7();
        }

        if (levelSelect == 8)
        {
            level8();
        }

        if (levelSelect == 9)
        {
            level9();
        }

        if (levelSelect == 10)
        {
            level10();
        }
        #endregion
    }

    #region Level 1-10
    public void level1()
    {
        string result = "25";
        textOutput.text = "Bitte löse folgende Aufgabe:\n5 * 5 = ?";
        buttonTextA.text = "25";
        buttonTextB.text = "30";
        buttonTextC.text = "20";
        buttonTextD.text = "35";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 2;
        }

    }

    private void level2()
    {
        string result = "21";
        textOutput.text = "Bitte löse folgende Aufgabe:\n3 * 7 = ?";
        buttonTextA.text = "14";
        buttonTextB.text = "35";
        buttonTextC.text = "21";
        buttonTextD.text = "28";

        if (result == buttonC.buttoninputC)
        {
            levelSelect = 3;
        }
    }

    private void level3()
    {
        string result = "36";
        textOutput.text = "Bitte löse folgende Aufgabe:\n6 * 6 = ?";
        buttonTextA.text = "48";
        buttonTextB.text = "30";
        buttonTextC.text = "42";
        buttonTextD.text = "36";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 4;
        }
    }

    private void level4()
    {
        string result = "72";
        textOutput.text = "Bitte löse folgende Aufgabe:\n9 * 8 = ?";
        buttonTextA.text = "80";
        buttonTextB.text = "72";
        buttonTextC.text = "64";
        buttonTextD.text = "56";

        if (result == buttonB.buttoninputB)
        {
            levelSelect = 5;
        }
    }

    private void level5()
    {
        string result = "78";
        textOutput.text = "Bitte löse folgende Aufgabe:\n13 * 6 = ?";
        buttonTextA.text = "78";
        buttonTextB.text = "65";
        buttonTextC.text = "91";
        buttonTextD.text = "52";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 6;
        }
    }

    private void level6()
    {
        string result = "84";
        textOutput.text = "Bitte löse folgende Aufgabe:\n12 * 7 = ?";
        buttonTextA.text = "63";
        buttonTextB.text = "91";
        buttonTextC.text = "70";
        buttonTextD.text = "84";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 7;
        }
    }

    private void level7()
    {
        string result = "80";
        textOutput.text = "Bitte löse folgende Aufgabe:\n16 * 5 = ?";
        buttonTextA.text = "70";
        buttonTextB.text = "85";
        buttonTextC.text = "80";
        buttonTextD.text = "75";

        if (result == buttonC.buttoninputC)
        {
            levelSelect = 8;
        }
    }

    private void level8()
    {
        string result = "99";
        textOutput.text = "Bitte löse folgende Aufgabe:\n33 * 3 = ?";
        buttonTextA.text = "99";
        buttonTextB.text = "96";
        buttonTextC.text = "103";
        buttonTextD.text = "93";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 9;
        }
    }

    private void level9()
    {
        string result = "100";
        textOutput.text = "Bitte löse folgende Aufgabe:\n10 * 10 = ?";
        buttonTextA.text = "80";
        buttonTextB.text = "110";
        buttonTextC.text = "90";
        buttonTextD.text = "100";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 10;
        }
    }

    private void level10()
    {
        string result = "78";
        textOutput.text = "Bitte löse folgende Aufgabe:\n26 * 3 = ?";
        buttonTextA.text = "75";
        buttonTextB.text = "77";
        buttonTextC.text = "78";
        buttonTextD.text = "80";

        if (result == buttonC.buttoninputC)
        {
            textOutput.text = "Herzlichen Glückwunsch du hast alle 10 Aufgaben gemeistert :D";
            buttonTextA.text = " ";
            buttonA.enabled = false;
            buttonTextB.text = " ";
            buttonB.enabled = false;
            buttonTextC.text = " ";
            buttonC.enabled = false;
            buttonTextD.text = " ";
            buttonD.enabled = false;
        }
    }
    #endregion
}
