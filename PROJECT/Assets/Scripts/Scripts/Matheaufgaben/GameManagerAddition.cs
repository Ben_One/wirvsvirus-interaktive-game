﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManagerAddition : MonoBehaviour
{
    #region SerializeField
    [SerializeField]
    TextMeshProUGUI textOutput;

    [SerializeField]
    ButtonA buttonA;

    [SerializeField]
    TextMeshProUGUI buttonTextA;

    [SerializeField]
    ButtonB buttonB;

    [SerializeField]
    TextMeshProUGUI buttonTextB;

    [SerializeField]
    ButtonC buttonC;

    [SerializeField]
    TextMeshProUGUI buttonTextC;

    [SerializeField]
    ButtonD buttonD;

    [SerializeField]
    TextMeshProUGUI buttonTextD;
    #endregion
    int levelSelect;

    void Start()
    {
        levelSelect = 1;
    }

    void Update()
    {
        #region Level Select
        if (levelSelect == 1)
        {
            level1();
        }

        if (levelSelect == 2)
        {
            level2();
        }

        if (levelSelect == 3)
        {
            level3();
        }

        if (levelSelect == 4)
        {
            level4();
        }

        if (levelSelect == 5)
        {
            level5();
        }

        if (levelSelect == 6)
        {
            level6();
        }

        if (levelSelect == 7)
        {
            level7();
        }

        if (levelSelect == 8)
        {
            level8();
        }

        if (levelSelect == 9)
        {
            level9();
        }

        if (levelSelect == 10)
        {
            level10();
        }
        #endregion
    }

    #region Level 1-10
    public void level1()
    {
        string result = "5";
        textOutput.text = "Bitte löse folgende Aufgabe:\n2 + 3 = ?";
        buttonTextA.text = "5";
        buttonTextB.text = "4";
        buttonTextC.text = "6";
        buttonTextD.text = "3";

        if(result == buttonA.buttoninputA)
        {
            levelSelect = 2;
        }

    }

    private void level2()
    {
        string result = "40";
        textOutput.text = "Bitte löse folgende Aufgabe:\n33 + 7 = ?";
        buttonTextA.text = "35";
        buttonTextB.text = "38";
        buttonTextC.text = "40";
        buttonTextD.text = "44";

        if (result == buttonC.buttoninputC)
        {
            levelSelect = 3;
        }
    }

    private void level3()
    {
        string result = "38";
        textOutput.text = "Bitte löse folgende Aufgabe:\n26 + 12 = ?";
        buttonTextA.text = "30";
        buttonTextB.text = "42";
        buttonTextC.text = "36";
        buttonTextD.text = "38";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 4;
        }
    }

    private void level4()
    {
        string result = "27";
        textOutput.text = "Bitte löse folgende Aufgabe:\n8 + 19 = ?";
        buttonTextA.text = "25";
        buttonTextB.text = "27";
        buttonTextC.text = "30";
        buttonTextD.text = "28";

        if (result == buttonB.buttoninputB)
        {
            levelSelect = 5;
        }
    }

    private void level5()
    {
        string result = "89";
        textOutput.text = "Bitte löse folgende Aufgabe:\n77 + 12 = ?";
        buttonTextA.text = "89";
        buttonTextB.text = "90";
        buttonTextC.text = "88";
        buttonTextD.text = "91";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 6;
        }
    }

    private void level6()
    {
        string result = "60";
        textOutput.text = "Bitte löse folgende Aufgabe:\n27 + 33 = ?";
        buttonTextA.text = "43";
        buttonTextB.text = "65";
        buttonTextC.text = "55";
        buttonTextD.text = "60";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 7;
        }
    }

    private void level7()
    {
        string result = "78";
        textOutput.text = "Bitte löse folgende Aufgabe:\n15 + 63 = ?";
        buttonTextA.text = "79";
        buttonTextB.text = "77";
        buttonTextC.text = "78";
        buttonTextD.text = "75";

        if (result == buttonC.buttoninputC)
        {
            levelSelect = 8;
        }
    }

    private void level8()
    {
        string result = "62";
        textOutput.text = "Bitte löse folgende Aufgabe:\n43 + 19 = ?";
        buttonTextA.text = "62";
        buttonTextB.text = "60";
        buttonTextC.text = "65";
        buttonTextD.text = "61";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 9;
        }
    }

    private void level9()
    {
        string result = "100";
        textOutput.text = "Bitte löse folgende Aufgabe:\n73 + 27 = ?";
        buttonTextA.text = "90";
        buttonTextB.text = "98";
        buttonTextC.text = "105";
        buttonTextD.text = "100";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 10;
        }
    }

    private void level10()
    {
        string result = "88";
        textOutput.text = "Bitte löse folgende Aufgabe:\n19 + 69 = ?";
        buttonTextA.text = "108";
        buttonTextB.text = "78";
        buttonTextC.text = "88";
        buttonTextD.text = "98";

        if (result == buttonC.buttoninputC)
        {
            textOutput.text = "Herzlichen Glückwunsch du hast alle 10 Aufgaben gemeistert :D";
            buttonTextA.text = " ";
            buttonA.enabled = false;
            buttonTextB.text = " ";
            buttonB.enabled = false;
            buttonTextC.text = " ";
            buttonC.enabled = false;
            buttonTextD.text = " ";
            buttonD.enabled = false;
        }
    }
    #endregion
}
