﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManagerDivision : MonoBehaviour
{
    #region SerializeField
    [SerializeField]
    TextMeshProUGUI textOutput;

    [SerializeField]
    ButtonA buttonA;

    [SerializeField]
    TextMeshProUGUI buttonTextA;

    [SerializeField]
    ButtonB buttonB;

    [SerializeField]
    TextMeshProUGUI buttonTextB;

    [SerializeField]
    ButtonC buttonC;

    [SerializeField]
    TextMeshProUGUI buttonTextC;

    [SerializeField]
    ButtonD buttonD;

    [SerializeField]
    TextMeshProUGUI buttonTextD;
    #endregion

    //Select Level
    int levelSelect;
    void Start()
    {
        levelSelect = 1;
    }

    void Update()
    {
        #region Level Select
        if (levelSelect == 1)
        {
            level1();
        }

        if (levelSelect == 2)
        {
            level2();
        }

        if (levelSelect == 3)
        {
            level3();
        }

        if (levelSelect == 4)
        {
            level4();
        }

        if (levelSelect == 5)
        {
            level5();
        }

        if (levelSelect == 6)
        {
            level6();
        }

        if (levelSelect == 7)
        {
            level7();
        }

        if (levelSelect == 8)
        {
            level8();
        }

        if (levelSelect == 9)
        {
            level9();
        }

        if (levelSelect == 10)
        {
            level10();
        }
        #endregion
    }

    #region Level 1-10
    public void level1()
    {
        string result = "7";
        textOutput.text = "Bitte löse folgende Aufgabe:\n35 : 5 = ?";
        buttonTextA.text = "7";
        buttonTextB.text = "6";
        buttonTextC.text = "8";
        buttonTextD.text = "5";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 2;
        }

    }

    private void level2()
    {
        string result = "2";
        textOutput.text = "Bitte löse folgende Aufgabe:\n6 : 3 = ?";
        buttonTextA.text = "4";
        buttonTextB.text = "3";
        buttonTextC.text = "2";
        buttonTextD.text = "1";

        if (result == buttonC.buttoninputC)
        {
            levelSelect = 3;
        }
    }

    private void level3()
    {
        string result = "9";
        textOutput.text = "Bitte löse folgende Aufgabe:\n81 : 9 = ?";
        buttonTextA.text = "10";
        buttonTextB.text = "8";
        buttonTextC.text = "7";
        buttonTextD.text = "9";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 4;
        }
    }

    private void level4()
    {
        string result = "6";
        textOutput.text = "Bitte löse folgende Aufgabe:\n36 : 6 = ?";
        buttonTextA.text = "7";
        buttonTextB.text = "6";
        buttonTextC.text = "4";
        buttonTextD.text = "8";

        if (result == buttonB.buttoninputB)
        {
            levelSelect = 5;
        }
    }

    private void level5()
    {
        string result = "15";
        textOutput.text = "Bitte löse folgende Aufgabe:\n75 : 5 = ?";
        buttonTextA.text = "15";
        buttonTextB.text = "17";
        buttonTextC.text = "12";
        buttonTextD.text = "16";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 6;
        }
    }

    private void level6()
    {
        string result = "10";
        textOutput.text = "Bitte löse folgende Aufgabe:\n100 : 10 = ?";
        buttonTextA.text = "7";
        buttonTextB.text = "9";
        buttonTextC.text = "11";
        buttonTextD.text = "10";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 7;
        }
    }

    private void level7()
    {
        string result = "4";
        textOutput.text = "Bitte löse folgende Aufgabe:\n28 : 7 = ?";
        buttonTextA.text = "5";
        buttonTextB.text = "6";
        buttonTextC.text = "4";
        buttonTextD.text = "7";

        if (result == buttonC.buttoninputC)
        {
            levelSelect = 8;
        }
    }

    private void level8()
    {
        string result = "13";
        textOutput.text = "Bitte löse folgende Aufgabe:\n78 : 6 = ?";
        buttonTextA.text = "13";
        buttonTextB.text = "11";
        buttonTextC.text = "14";
        buttonTextD.text = "12";

        if (result == buttonA.buttoninputA)
        {
            levelSelect = 9;
        }
    }

    private void level9()
    {
        string result = "13";
        textOutput.text = "Bitte löse folgende Aufgabe:\n91 : 7 = ?";
        buttonTextA.text = "12";
        buttonTextB.text = "17";
        buttonTextC.text = "14";
        buttonTextD.text = "13";

        if (result == buttonD.buttoninputD)
        {
            levelSelect = 10;
        }
    }

    private void level10()
    {
        string result = "17";
        textOutput.text = "Bitte löse folgende Aufgabe:\n51 : 3 = ?";
        buttonTextA.text = "12";
        buttonTextB.text = "19";
        buttonTextC.text = "17";
        buttonTextD.text = "15";

        if (result == buttonC.buttoninputC)
        {
            textOutput.text = "Herzlichen Glückwunsch du hast alle 10 Aufgaben gemeistert :D";
            buttonTextA.text = " ";
            buttonA.enabled = false;
            buttonTextB.text = " ";
            buttonB.enabled = false;
            buttonTextC.text = " ";
            buttonC.enabled = false;
            buttonTextD.text = " ";
            buttonD.enabled = false;
        }
    }
    #endregion
}
