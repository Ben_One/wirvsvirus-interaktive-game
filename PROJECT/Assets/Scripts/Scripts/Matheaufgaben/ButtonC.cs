﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonC : MonoBehaviour
{
    public string buttoninputC;

    public void ButtonInput()
    {
        buttoninputC = GetComponentInChildren<TextMeshProUGUI>().text;
    }
}
