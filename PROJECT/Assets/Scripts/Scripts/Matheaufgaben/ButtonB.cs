﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonB : MonoBehaviour
{
    public string buttoninputB;

    public void ButtonInput()
    {
        buttoninputB = GetComponentInChildren<TextMeshProUGUI>().text;
    }
}
