﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    private static System.Random s_rnd = new System.Random();

    public static T RandomElement<T>(this List<T> _list)
    {
        return _list[s_rnd.Next(0, _list.Count)];
    }
}
