﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSettings
{
    public static GlobalSettings Instance
    {
        get
        {
            if (s_instance is null)
            {
                s_instance = new GlobalSettings();
            }
            return s_instance;
        }
    }

    private static GlobalSettings s_instance;

    public bool IsMurmeltTierActive { get; private set; }

    private GlobalSettings()
    {
        IsMurmeltTierActive = PlayerPrefs.GetInt("MurmelTierModues", 0) == 1 ? true : false;
    }

}
