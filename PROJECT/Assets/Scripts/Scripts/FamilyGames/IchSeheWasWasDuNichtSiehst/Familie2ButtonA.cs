﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Familie2ButtonA : MonoBehaviour
{
    [SerializeField]
    TMP_InputField Input;

    [SerializeField]
    TextMeshProUGUI ButtonText;

    [SerializeField]
    ISWWDNSManager gameManager;

    public string selctedInput;
    public bool inputIsSet = false;
    public bool erraten = false;

    public void Ok()
    {
        selctedInput = Input.text;
        inputIsSet = true;
    }

    public void NextRound()
    {
        if(ButtonText.text == "Erraten")
        {
            gameManager.StartInfo();
        }
    }


    private void Update()
    {
        //Debug.Log(selctedInput);
    }
}
