﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ISWWDNSManager : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI TextOutput;

    [SerializeField]
    Familie2Input Inputfield;

    [SerializeField]
    Familie2ButtonA ButtonA;

    [SerializeField]
    TextMeshProUGUI TextButtonA;

    [SerializeField]
    Familie2ButtonB ButtonB;

    [SerializeField]
    TextMeshProUGUI TextButtonB;


    void Start()
    {
        StartInfo();
    }

    void Update()
    {
        Debug.Log(ButtonA.erraten);

        if (ButtonA.inputIsSet)
        {
            TextOutput.text = "Beschreibe deinem Mitspieler deinen Gegenstand den du eingeben hast.\nwenn er eraten wurde Klicke auf Zurück";
            Inputfield.gameObject.SetActive(false);
            TextButtonA.text = "Erraten";
            ButtonA.gameObject.SetActive(false);
        }
        if (ButtonA.erraten)
        {
            ButtonA.inputIsSet = false;
            StartInfo();
        }
    }

    public void StartInfo()
    {
        TextOutput.text = "Dies ist  ein Klassisches:\n Ich sehe was was du nicht siehst.\n\nSuche dir etwas in deiner Umgebung aus und tippe es unten ein." +
            "\n\nBestätige danach deine eingabe mit dem OK Knopf";
        TextButtonA.text = "OK";
        ButtonB.gameObject.SetActive(false);
    }


}
