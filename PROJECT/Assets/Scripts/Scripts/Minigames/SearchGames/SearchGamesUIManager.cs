﻿using Minigames.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Minigames.SearchGames
{
    [DisallowMultipleComponent]
    public class SearchGamesUIManager : MonoBehaviour
    {
        public static SearchGamesUIManager Instance { get; private set; }

        [SerializeField]
        private string m_noActiveTaskMessage = "Keine aktive Aufgabe!";
        [SerializeField]
        private string m_getNewActiveTaskMessage = "Nächste Aufgabe!";
        [SerializeField]
        private string m_solveActiveTaskMessage = "Gelöst!";
        [SerializeField]
        private string m_noMoreTasksMessage = "Keine weitere Aufgaben!";
        [SerializeField]
        private CustomTypes.Scene m_mainMenuScene;

        [SerializeField]
        private TextMeshProUGUI m_header;
        [SerializeField]
        private TextMeshProUGUI m_description;
        [SerializeField]
        private TextMeshProUGUI m_buttonText;

        private void Awake()
        {
            if (Instance is object && Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
            Instance = this;
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        public void NextTask()
        {
            SearchGamesManager.Instance.StartNextTask();
        }

        public void DisplayTask(TaskData _data)
        {
            if (_data is object)
            {
                m_description.text = _data.Description;
                m_header.text = _data.Titel;
                m_buttonText.text = m_solveActiveTaskMessage;
            }
            else
            {
                m_description.text = m_noActiveTaskMessage;
                m_header.text = string.Empty;
                if (SearchGamesManager.Instance.AllTasksSolved)
                {
                    m_buttonText.text = m_noMoreTasksMessage;
                }
                else
                {
                    m_buttonText.text = m_getNewActiveTaskMessage;
                }
            }
        }

        public void SolvedTask()
        {
            if (SearchGamesManager.Instance.ActiveTask is object)
            {
                SearchGamesManager.Instance.SolvedActiveTask();
                m_buttonText.text = m_getNewActiveTaskMessage;
            }
            else
            {
                SearchGamesManager.Instance.StartNextTask();
                m_buttonText.text = m_solveActiveTaskMessage;
            }
        }

        public void BackToMenu()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(m_mainMenuScene);
        }
    }
}