﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Minigames.Data;
using System.Xml.Serialization;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Minigames.SearchGames
{
    [DefaultExecutionOrder(-50)]
    [DisallowMultipleComponent]
    [AddComponentMenu("Minigames/SearchGames/Search Games Manager")]
    public class SearchGamesManager : MonoBehaviour
    {
        public static SearchGamesManager Instance { get; private set; }

        public TaskData ActiveTask
        {
            get
            {
                return m_activeTask;
            }
            private set
            {
                m_activeTask = value;
                SearchGamesUIManager.Instance.DisplayTask(m_activeTask);
            }
        }

        public bool AllTasksSolved
        {
            get
            {
                return m_tasks.Count == 0;
            }
        }


        [SerializeField]
        private string m_folderName = "SearchGames";
        [SerializeField]
        private string m_taskFileName = "AllTasks.xml";
        [SerializeField]
        private string m_activeTaskFileName = "ActiveTask.xml";
        [SerializeField]
        private string m_lastVisitedFileName = "LastVisited.xml";
        [SerializeField]
        private int m_scorePerSolve = 5;
        [SerializeField]
        private CustomTypes.Scene m_scoreScene;

        private List<TaskData> m_tasks = new List<TaskData>();
        private List<TaskData> m_solvedTasks = new List<TaskData>();
        private TaskData m_activeTask;
        private System.DateTime m_lastVisit;

        private void Awake()
        {
            if (Instance is object && Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
            Instance = this;
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                SaveTaskState();
                SaveLastVisitTime();
                SaveActiveTask();
                Instance = null;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            LoadLastVisitTime();
            LoadTasks();
            LoadActiveTask();

            bool isMissingScoreScene = true;
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                if (SceneManager.GetSceneAt(i).path == m_scoreScene.Path)
                {
                    isMissingScoreScene = false;
                }
            }
            if (isMissingScoreScene)
            {
                LoadSceneParameters parameters = new LoadSceneParameters(LoadSceneMode.Additive, LocalPhysicsMode.None);
                SceneManager.LoadScene(m_scoreScene, parameters);
            }
        }

        private void OnApplicationQuit()
        {
            SaveTaskState();
            SaveLastVisitTime();
            SaveActiveTask();
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                SaveTaskState();
                SaveLastVisitTime();
                SaveActiveTask();
            }
        }

        public void StartNextTask()
        {
            if (m_tasks.Count == 0)
            {
                ActiveTask = null;
                return;
            }

            ActiveTask = m_tasks.RandomElement();

            m_tasks.Remove(m_activeTask);
        }

        public void SolvedActiveTask()
        {
            m_solvedTasks.Add(m_activeTask);
            m_activeTask.IsSolved = true;
            ScoreManager.Instance.IncreaseScore(m_scorePerSolve);
            ActiveTask = null;
        }

        private void SaveTaskState()
        {
            List<TaskData> allTasks = new List<TaskData>();
            allTasks.AddRange(m_tasks);
            allTasks.AddRange(m_solvedTasks);

            string path = Path.Combine(Application.persistentDataPath, m_folderName, m_taskFileName);
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<TaskData>));
                serializer.Serialize(fs, allTasks);
            }
        }

        private void SaveLastVisitTime()
        {
            string path = Path.Combine(Application.persistentDataPath, m_folderName, m_lastVisitedFileName);
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(System.DateTime));
                serializer.Serialize(fs, m_lastVisit);
            }
        }

        private void SaveActiveTask()
        {
            string path = Path.Combine(Application.persistentDataPath, m_folderName, m_activeTaskFileName);

            if (File.Exists(path) && m_activeTask is null)
            {
                File.Delete(path);
                return;
            }

            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TaskData));
                serializer.Serialize(fs, m_activeTask);
            }
        }

        private void LoadLastVisitTime()
        {
            string path = Path.Combine(Application.persistentDataPath, m_folderName, m_lastVisitedFileName);

            if (!Directory.Exists(Path.Combine(Application.persistentDataPath, m_folderName)))
            {
                Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, m_folderName));
            }

            if (!File.Exists(path))
            {
                m_lastVisit = System.DateTime.Now;
            }
            else
            {
                using (FileStream fs = File.OpenRead(path))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(System.DateTime));
                    m_lastVisit = (System.DateTime)serializer.Deserialize(fs);
                }
            }
        }

        private void LoadTasks()
        {
            string path = Path.Combine(Application.persistentDataPath, m_folderName, m_taskFileName);

            if (!Directory.Exists(Path.Combine(Application.persistentDataPath, m_folderName)))
            {
                Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, m_folderName));
            }
            // Move file to persisten storage
            if (!File.Exists(path))
            {
                string streamPath = Path.Combine(Application.streamingAssetsPath, m_folderName, m_taskFileName);
                string taskXML = MobileUtilities.StorageUtility.LoadFromStreamingAssets(streamPath);

                using (FileStream fs = File.Create(path))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.Write(taskXML);
                    }
                }
            }
            using (FileStream fs = File.OpenRead(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<TaskData>));
                m_tasks = (List<TaskData>)serializer.Deserialize(fs);
            }

            if (GlobalSettings.Instance.IsMurmeltTierActive)
            {
                if ((System.DateTime.Now.Day - m_lastVisit.Day) != 0)
                {
                    // Set all task as unsolved
                    foreach (TaskData task in m_tasks)
                    {
                        task.IsSolved = false;
                    }
                }
            }
            // Remove already solved task from the pool
            foreach (TaskData task in m_tasks)
            {
                if (task.IsSolved)
                {
                    m_solvedTasks.Add(task);
                }
            }

            foreach (TaskData task in m_solvedTasks)
            {
                m_tasks.Remove(task);
            }
        }

        private void LoadActiveTask()
        {
            string path = Path.Combine(Application.persistentDataPath, m_folderName, m_activeTaskFileName);

            if (!Directory.Exists(Path.Combine(Application.persistentDataPath, m_folderName)))
            {
                Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, m_folderName));
            }

            if (!File.Exists(path))
            {
                ActiveTask = null;
            }
            else
            {
                using (FileStream fs = File.OpenRead(path))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(TaskData));
                    ActiveTask = (TaskData)serializer.Deserialize(fs);
                }
            }
        }

    }
}
