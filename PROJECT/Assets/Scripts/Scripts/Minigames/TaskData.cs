﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace Minigames.Data
{
    [System.Flags]
    public enum ESpecialRequirements
    {
        NONE = 0,
        IMAGE_REQUIRED = 1 << 0,
        TIME_TRACKING = 1 << 1,
        ATLEAST_TWO_PERSONS = 1 << 2,
    }
    public class TaskData
    {
        public ESpecialRequirements SpecialRequirements { get; set; }
        public int Difficulty { get; set; }
        public string Titel { get; set; }
        public string Description { get; set; }
        public bool IsSolved { get; set; }

        public TaskData()
        {

        }

        public static TaskData LoadTask(string _path)
        {
            using (FileStream fs = File.OpenRead(_path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TaskData));
                return (TaskData)serializer.Deserialize(fs);
            }
        }

        public override string ToString()
        {
            return $"{Titel}[{Difficulty}] - {Description}";
        }
    }
}
