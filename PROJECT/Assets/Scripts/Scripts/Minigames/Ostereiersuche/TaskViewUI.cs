﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Minigames.OstereierSuche
{
    public class TaskViewUI : MonoBehaviour
    {
        public OstereierTask AssociatedTask
        {
            get
            {
                return m_associatedTask;
            }
            set
            {
                m_associatedTask = value;
                if (m_associatedTask is null)
                    return;

                if (m_associatedTask.Type == EOStereierTaskType.IMAGE)
                {
                    m_taskImage.texture = m_associatedTask.TaskImage;
                    m_taskImage.gameObject.SetActive(true);
                    m_taskDescription.gameObject.SetActive(false);
                }
                else
                {
                    m_taskDescription.text = m_associatedTask.TaskDescription;
                    m_taskDescription.gameObject.SetActive(true);
                    m_taskImage.gameObject.SetActive(false);
                }
            }
        }

        public int TaskNumber
        {
            set
            {
                m_header.text = string.Format(HEADER_FORMAT, value);
            }
        }

        [SerializeField]
        private RawImage m_taskImage;
        [SerializeField]
        private TextMeshProUGUI m_taskDescription;
        [SerializeField]
        private TextMeshProUGUI m_header;

        private const string HEADER_FORMAT = "Aufgabe {0}";

        private OstereierTask m_associatedTask;

        public void RemoveTask()
        {
            OstereierManager.Instance.RemoveTask(AssociatedTask);
            OstereierUIManager.Instance.RemoveTask(AssociatedTask);
            Destroy(this.gameObject);
        }
    }
}
