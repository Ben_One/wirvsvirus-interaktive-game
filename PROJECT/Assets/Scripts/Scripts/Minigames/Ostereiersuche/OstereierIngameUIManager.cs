﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Minigames.OstereierSuche
{
    public class OstereierIngameUIManager : MonoBehaviour
    {
        public static OstereierIngameUIManager Instance { get; private set; }

        [SerializeField]
        private RectTransform m_taskScreenPanel;
        [SerializeField]
        private TaskViewUI m_taskViewUI;
        [SerializeField]
        private RectTransform m_keepImagePanel;
        [SerializeField]
        private RectTransform m_returnImagePanel;

        private void Awake()
        {
            if (Instance is object && Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
            Instance = this;
        }

        private void Start()
        {
            StartCoroutine(OstereierManager.Instance.TakePictureForObject());
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        public void CloseTaskView()
        {
            m_taskScreenPanel.gameObject.SetActive(false);
        }

        public void OpenTaskView()
        {
            m_taskViewUI.AssociatedTask = OstereierManager.Instance.GetActiveTask();
            m_taskViewUI.TaskNumber = OstereierManager.Instance.GetActiveTaskIndex();
            m_taskScreenPanel.gameObject.SetActive(true);
        }

        public void TakePicture()
        {
            OstereierManager.Instance.TakePictureAsHint();
            m_keepImagePanel.gameObject.SetActive(true);
            m_returnImagePanel.gameObject.SetActive(false);
        }

        public void KeepImage()
        {
            OstereierManager.Instance.KeepImageAsSolution();
            m_keepImagePanel.gameObject.SetActive(false);
            m_returnImagePanel.gameObject.SetActive(true);
        }

        public void DiscardImage()
        {
            OstereierManager.Instance.DiscardImageAsHint();
            m_keepImagePanel.gameObject.SetActive(false);
            m_returnImagePanel.gameObject.SetActive(true);
        }
    }
}
