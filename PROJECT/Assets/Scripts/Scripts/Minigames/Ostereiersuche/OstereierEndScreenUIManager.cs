﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Minigames.OstereierSuche
{
    public class OstereierEndScreenUIManager : MonoBehaviour
    {
        [SerializeField]
        private TaskViewUI m_taskViewUI;
        [SerializeField]
        private TextMeshProUGUI m_headerText;
        [SerializeField]
        private RawImage m_taskImage;

        private int m_currentTask = 0;
        private bool m_nextIsSolution = false;

        private void Start()
        {
            DisplayNext();
        }

        public void DisplayNext()
        {
            if (m_currentTask == OstereierManager.Instance.Solutions.Count)
            {
                OstereierManager.Instance.Clear();
                OstereierManager.Instance.LoadMenu();
                return;
            }
            if (m_nextIsSolution)
            {
                // Display Solution
                m_taskViewUI.AssociatedTask = OstereierManager.Instance.Solutions[m_currentTask];
                m_headerText.text = "Du hast das gefunden:";
                m_currentTask = m_currentTask + 1;
            }
            else
            {
                // Display Task
                m_taskViewUI.AssociatedTask = OstereierManager.Instance.Tasks[m_currentTask];
                m_headerText.text = "Du hättest dies finden sollen:";
            }
            m_nextIsSolution = !m_nextIsSolution;
        }
        public void DisplayPrevious()
        {
            if (m_nextIsSolution)
            {
                // Display Solution
                m_taskViewUI.AssociatedTask = OstereierManager.Instance.Solutions[m_currentTask];
                m_headerText.text = "Du hast das gefunden:";
            }
            else
            {
                // Display Task
                m_taskViewUI.AssociatedTask = OstereierManager.Instance.Tasks[m_currentTask];
                m_headerText.text = "Du hättest dies finden sollen:";
                m_currentTask = Mathf.Max(0, m_currentTask - 1);
            }
            m_nextIsSolution = !m_nextIsSolution;
        }
    }
}
