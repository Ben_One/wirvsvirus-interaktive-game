﻿using UnityEngine;

namespace Minigames.OstereierSuche
{
    public enum EOStereierTaskType
    {
        NONE,
        IMAGE,
        TEXT
    }

    [System.Serializable]
    public class OstereierTask
    {
        public EOStereierTaskType Type { get; private set; } = EOStereierTaskType.NONE;
        public string TaskDescription { get; private set; }
        public Texture2D TaskImage { get; private set; }

        public OstereierTask(string _description)
        {
            Type = EOStereierTaskType.TEXT;
            TaskDescription = _description;
        }

        public OstereierTask(Texture2D _image)
        {
            Type = EOStereierTaskType.IMAGE;
            TaskImage = _image;
        }
    }
}