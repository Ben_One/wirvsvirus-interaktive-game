﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Minigames.OstereierSuche
{
    public class OstereierUIManager : MonoBehaviour
    {
        public static OstereierUIManager Instance { get; private set; }

        [SerializeField]
        private float m_heightPerImageTask = 300;
        [SerializeField]
        private float m_heightPerTextTask = 100;

        [SerializeField]
        private RectTransform m_takeImagePanel;
        [SerializeField]
        private RectTransform m_takeImageReturnPanel;
        [SerializeField]
        private RectTransform m_keepImagePanel;
        [SerializeField]
        private TMP_InputField m_taskInputField;
        [SerializeField]
        private RectTransform m_taskViewPanel;
        [SerializeField]
        private RectTransform m_taskContentPanel;
        [SerializeField]
        private TaskViewUI m_taskViewPrefab;
        [SerializeField]
        private Button m_startButton;

        [SerializeField]
        private CustomTypes.Scene m_mainMenu;

        private void Awake()
        {
            if (Instance is object && Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
            Instance = this;
        }


        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        public void StartTakeObjectPicture()
        {
            StartCoroutine(OstereierManager.Instance.TakePictureForObject());
        }

        public void StartGame()
        {
            OstereierManager.Instance.StartGame();
        }

        public void DisplayTakeImagePanel()
        {
            m_takeImagePanel.gameObject.SetActive(true);
        }

        public void TakePictureAsHint()
        {
            OstereierManager.Instance.TakePictureAsHint();
            m_keepImagePanel.gameObject.SetActive(true);
            m_takeImageReturnPanel.gameObject.SetActive(false);
        }

        public void KeepImageAsHint()
        {
            OstereierManager.Instance.KeepImageAsHint();
            m_keepImagePanel.gameObject.SetActive(false);
            m_takeImageReturnPanel.gameObject.SetActive(true);

            m_startButton.interactable = true;
        }

        public void DiscardImageAsHint()
        {
            OstereierManager.Instance.DiscardImageAsHint();
            m_keepImagePanel.gameObject.SetActive(false);
            m_takeImageReturnPanel.gameObject.SetActive(true);
        }

        public void ReturnFromTakeImage()
        {
            m_takeImagePanel.gameObject.SetActive(false);
            OstereierManager.Instance.StopCamera();
        }

        public void ViewTasks()
        {
            TaskViewUI taskViewUI;
            Vector2 contentSizeDelta = Vector2.zero;
            int taskNumber = 1;
            foreach (OstereierTask task in OstereierManager.Instance.Tasks)
            {
                if (task.Type == EOStereierTaskType.NONE)
                    continue;

                taskViewUI = Instantiate(m_taskViewPrefab);
                taskViewUI.transform.SetParent(m_taskContentPanel, false);

                taskViewUI.AssociatedTask = task;
                taskViewUI.TaskNumber = taskNumber;

                if (task.Type == EOStereierTaskType.IMAGE)
                {
                    contentSizeDelta.y += m_heightPerImageTask;
                }
                else
                {
                    contentSizeDelta.y += m_heightPerTextTask;
                }

                taskNumber++;
            }

            m_taskContentPanel.sizeDelta = contentSizeDelta;
            m_taskViewPanel.gameObject.SetActive(true);
        }

        public void RemoveTask(OstereierTask _task)
        {
            Vector2 contentSizeDelta = m_taskContentPanel.sizeDelta;
            if (_task.Type == EOStereierTaskType.IMAGE)
            {
                contentSizeDelta.y -= m_heightPerImageTask;
            }
            else
            {
                contentSizeDelta.y -= m_heightPerTextTask;
            }

            m_taskContentPanel.sizeDelta = contentSizeDelta;

            if (OstereierManager.Instance.Tasks.Count == 0)
            {
                m_startButton.interactable = false;
            }
        }

        public void CloseTaskView()
        {
            for (int i = 0; i < m_taskContentPanel.childCount; i++)
            {
                Destroy(m_taskContentPanel.GetChild(i).gameObject);
            }
            m_taskViewPanel.gameObject.SetActive(false);
        }

        public void AddNew()
        {
            if (string.IsNullOrWhiteSpace(m_taskInputField.text))
                return;

            OstereierManager.Instance.AddTextTask(m_taskInputField.text);
            m_taskInputField.text = string.Empty;
            m_startButton.interactable = true;
        }

        public void BackToMainMenu()
        {
            Destroy(OstereierManager.Instance.gameObject);
            UnityEngine.SceneManagement.SceneManager.LoadScene(m_mainMenu.Path);
        }
    }
}
