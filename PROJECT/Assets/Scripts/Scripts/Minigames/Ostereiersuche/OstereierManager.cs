﻿using MobileUtilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Minigames.OstereierSuche
{
    public class OstereierManager : MonoBehaviour
    {
        public static OstereierManager Instance { get; private set; }

        public Texture ObjectToFindImage { get; set; }
        public string ObjectToFindText { get; set; }
        public List<OstereierTask> Tasks
        {
            get
            {
                return new List<OstereierTask>(m_ostereierTasks);
            }
        }
        public List<OstereierTask> Solutions
        {
            get
            {
                return new List<OstereierTask>(m_ostereierSolutions);
            }
        }

        [SerializeField]
        private Material m_cameraMaterial;
        [SerializeField]
        private CustomTypes.Scene m_gameMenuScreen;
        [SerializeField]
        private CustomTypes.Scene m_gameScene;
        [SerializeField]
        private CustomTypes.Scene m_gameEndScreen;
        [SerializeField]
        private CustomTypes.Scene m_scoreScene;

        public List<OstereierTask> m_ostereierTasks = new List<OstereierTask>();
        public List<OstereierTask> m_ostereierSolutions = new List<OstereierTask>();

        private Texture m_tempImage;

        private WebCamTexture m_backFaceCam;

        private readonly string m_requestReason = "Dieses Minispiel nutzt die Kamera, um Fotos von Objekten zu machen, welche dann gefunden werden sollen!";

        private int m_activeTaskID = 0;

        private void Awake()
        {
            if (Instance is object && Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }
            Instance = this;
            DontDestroyOnLoad(this.gameObject);

            SceneManager.activeSceneChanged += CheckIfManagerCanBeDestroyed;
        }

        private void Start()
        {
            if (!CameraUtility.HasPermission)
            {
                CameraUtility.RequestPermission(m_requestReason);
            }
            bool isMissingScoreScene = true;
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                if (SceneManager.GetSceneAt(i).path == m_scoreScene.Path)
                {
                    isMissingScoreScene = false;
                }
            }
            if (isMissingScoreScene)
            {
                LoadSceneParameters parameters = new LoadSceneParameters(LoadSceneMode.Additive, LocalPhysicsMode.None);
                SceneManager.LoadScene(m_scoreScene.Path, parameters);
            }
        }

        private void CheckIfManagerCanBeDestroyed(Scene _old, Scene _new)
        {
            //if (!_new.name.ToLower().Contains("ostereier"))
            //{
            //    Destroy(this.gameObject);
            //}
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
                StopCamera();
            }
        }

        public void StartGame()
        {
            if (!CameraUtility.HasPermission)
            {
                CameraUtility.RequestPermission(m_requestReason);
            }
            else
            {
                SceneManager.LoadScene(m_gameScene.Name);
            }
        }

        #region ---Preparation---

        public IEnumerator TakePictureForObject()
        {
            if (!CameraUtility.HasPermission)
            {
                CameraUtility.RequestPermission(m_requestReason);
                yield return new WaitForSeconds(5);
            }

            if (!CameraUtility.HasPermission)
                yield break;

            if (m_backFaceCam is null)
            {
#if UNITY_EDITOR
                // Uses a attached web cam to the computer
                m_backFaceCam = CameraUtility.GetFrontFacingCamera();
#else
                m_backFaceCam = CameraUtility.GetBackFacingCamera();
#endif
            }
            m_backFaceCam.Play();

            m_cameraMaterial.mainTexture = m_backFaceCam;

            OstereierUIManager.Instance?.DisplayTakeImagePanel();
        }

        public void TakePictureAsHint()
        {
            m_backFaceCam.Pause();
        }

        public void KeepImageAsHint()
        {
            Texture2D image = new Texture2D(m_backFaceCam.width, m_backFaceCam.height);
            image.SetPixels32(m_backFaceCam.GetPixels32());
            image.Apply();

            m_ostereierTasks.Add(new OstereierTask(image));
            m_backFaceCam.Play();
        }

        public void DiscardImageAsHint()
        {
            m_backFaceCam.Play();
        }

        public void StopCamera()
        {
            m_backFaceCam?.Stop();
        }

        public void AddTextTask(string _task)
        {
            if (m_ostereierTasks.FirstOrDefault(o => o.Type == EOStereierTaskType.TEXT && o.TaskDescription == _task) is object)
            {
                // already added
#if DEBUG
                Debug.LogWarning($"The task {_task} has already been added!");
#endif
                return;
            }
            m_ostereierTasks.Add(new OstereierTask(_task));
        }

        public void RemoveTask(OstereierTask _task)
        {
            m_ostereierTasks.Remove(_task);
        }
        #endregion

        #region ---MainGame---
        public OstereierTask GetActiveTask()
        {
            if (m_activeTaskID >= 0 && m_activeTaskID < m_ostereierTasks.Count)
            {
                return m_ostereierTasks[m_activeTaskID];
            }
            return null;
        }

        public int GetActiveTaskIndex()
        {
            return m_activeTaskID + 1;
        }

        public void KeepImageAsSolution()
        {
            Texture2D image = new Texture2D(m_backFaceCam.width, m_backFaceCam.height);
            image.SetPixels32(m_backFaceCam.GetPixels32());
            image.Apply();

            m_ostereierSolutions.Add(new OstereierTask(image));
            m_backFaceCam.Play();

            m_activeTaskID++;

            if (m_activeTaskID >= m_ostereierTasks.Count)
            {
                // all tasks solved
                SceneManager.LoadScene(m_gameEndScreen.Path);
            }
        }

        public void LoadMenu()
        {
            SceneManager.LoadScene(m_gameMenuScreen.Path);
        }

        public void Clear()
        {
            m_ostereierSolutions.Clear();
            m_ostereierTasks.Clear();
            m_activeTaskID = 0;
        }
        #endregion
    }
}
