﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ScoreManagment.UI
{
    public class ScoreUIManager : MonoBehaviour
    {
        [SerializeField]
        private string m_levelFormat = "Level {0}";
        [SerializeField]
        private Slider m_levelProgress;
        [SerializeField]
        private TextMeshProUGUI m_levelText;

        // Start is called before the first frame update
        void Start()
        {
            m_levelText.text = string.Format(m_levelFormat, ScoreManager.Instance.CurrentLevel);
            m_levelProgress.value = ScoreManager.Instance.ScorePercentage;
            ScoreManager.Instance.AddToLevelUpEvent(LevelChanged);
            ScoreManager.Instance.AddToScoreAddedEvent(ScoreChanged);
        }

        private void LevelChanged(int _currentLevel)
        {
            m_levelText.text = string.Format(m_levelFormat, _currentLevel);
            m_levelProgress.value = ScoreManager.Instance.ScorePercentage;
        }

        private void ScoreChanged(int _currentScore, int _difference)
        {
            m_levelProgress.value = ScoreManager.Instance.ScorePercentage;
        }
    }
}