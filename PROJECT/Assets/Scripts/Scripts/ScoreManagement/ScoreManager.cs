﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager
{
    public delegate void LevelUpDelegate(int _newLevel);
    public delegate void ScoreAddedDelegate(int _newScore, int _add);

    public static ScoreManager Instance
    {
        get
        {
            if (s_instance is null)
            {
                s_instance = new ScoreManager();
            }
            return s_instance;
        }
    }

    private static ScoreManager s_instance;

    public int[] AllowedDifficulties
    {
        get
        {
            if (m_currentLevel >= 5)
            {
                return new int[] { 1, 2, 3 };
            }
            else if (m_currentLevel >= 3)
            {
                return new int[] { 1, 2 };
            }
            else
            {
                return new int[] { 1 };
            }
        }
    }

    public int CurrentScore
    {
        get
        {
            return m_currentScore;
        }
        private set
        {
            int difference = value - m_currentLevel;
            m_currentScore = value;
            PlayerPrefs.SetInt("CurrentScore", m_currentLevel);
            PlayerPrefs.Save();
            m_onScoreAdded?.Invoke(m_currentScore, difference);

            if (m_currentScore >= NeededScoreForLevelUp)
            {
                m_baseLevelScore = NeededScoreForLevelUp;
                CurrentLevel++;
            }
        }
    }

    public int CurrentLevel
    {
        get
        {
            return m_currentLevel;
        }
        private set
        {
            m_currentLevel = value;
            PlayerPrefs.SetInt("CurrentLevel", m_currentLevel);
            PlayerPrefs.Save();

            m_onLevelUp?.Invoke(m_currentLevel);
        }
    }

    public int NeededScoreForLevelUp
    {
        get
        {
            if (m_currentLevel >= 5)
                return m_baseLevelScore + 30;
            if (m_currentLevel >= 3)
                return m_baseLevelScore + 20;
            return m_baseLevelScore + 10;
        }
    }

    public int BaseLevelScore
    {
        get
        {
            return m_baseLevelScore;
        }
    }

    public float ScorePercentage
    {
        get
        {
            return (float)(m_currentScore - m_baseLevelScore) / (NeededScoreForLevelUp - m_baseLevelScore);
        }
    }

    private event LevelUpDelegate m_onLevelUp;
    private event ScoreAddedDelegate m_onScoreAdded;

    private int m_baseLevelScore;
    private int m_currentScore;
    private int m_currentLevel;

    private ScoreManager()
    {
#if DEBUG
        m_onLevelUp += (level) => Debug.Log("LevelUP:" + level);
        m_onScoreAdded += (score, diff) => Debug.Log("Current Score: " + score);
#endif
        CurrentLevel = PlayerPrefs.GetInt("CurrentLevel", 1);
        CurrentScore = PlayerPrefs.GetInt("CurrentScore", 0);
    }

    public void IncreaseScore(int _score)
    {
        CurrentScore += _score;
    }

    public void AddToLevelUpEvent(LevelUpDelegate _func)
    {
        m_onLevelUp -= _func;
        m_onLevelUp += _func;
    }

    public void RemoveFromLevelUpEvent(LevelUpDelegate _func)
    {
        m_onLevelUp -= _func;
    }

    public void AddToScoreAddedEvent(ScoreAddedDelegate _func)
    {
        m_onScoreAdded -= _func;
        m_onScoreAdded += _func;
    }

    public void RemoveFromScoreAddedEvent(ScoreAddedDelegate _func)
    {
        m_onScoreAdded -= _func;
    }
}
